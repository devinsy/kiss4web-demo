/**
 * Copyright (C) 2016 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web-demo.
 * 
 * Kiss4web-demo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Kiss4web-demo.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.demo.website.xidyn;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;

import fr.devinsy.kiss4web.demo.website.FatalView;
import fr.devinsy.util.strings.StringList;
import fr.devinsy.xidyn.data.SimpleTagData;
import fr.devinsy.xidyn.pages.Page;
import fr.devinsy.xidyn.pages.PageFactory;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class Index_xhtml extends HttpServlet
{
    private static final long serialVersionUID = 1513508164890999104L;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Index_xhtml.class);

    /**
     *
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
    {
        logger.debug("doGet starting…");

        try
        {
            //
            logger.debug("doGet starting…");

            // Get parameters.
            // ===============
            // Locale locale = kiwa.getUserLocale(request);
            // Long accountId = kiwa.getAuthentifiedAccountId(request,
            // response);

            // Use parameters.
            // ===============

            // Send response.
            // ==============
            Page demoPage = PageFactory.instance().create("/fr/devinsy/kiss4web/demo/website/xidyn/index.html");

            // Following examples use inline HTML code rather to use the
            // index.html HTML code. It is because each one are autonomous of
            // any outside context.

            // Step #01.
            {
                try
                {
                    String htmlSource = "<div id=\"pseudo\">a pseudo</div>";

                    Page page = new Page(htmlSource);
                    page.setContent("pseudo", "Superman");

                    String htmlTarget = page.dynamizeToString();

                    StringList display = new StringList();
                    display.append(StringEscapeUtils.escapeXml(htmlSource)).appendln("<br/>");
                    display.append("+").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent(\"pseudo\", \"Superman\");")).appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget)).appendln("<br/>");

                    demoPage.setContent("demo1", display.toString());
                }
                catch (Exception exception)
                {
                    demoPage.setContent("demo1", exception.getMessage());
                }
            }

            // Step #02.
            {
                try
                {
                    String htmlSource = "<div id=\"pseudo\">a pseudo</div>";

                    Page page = new Page(htmlSource);
                    page.setContent("pseudo", "Spiderman");
                    page.appendAttribute("pseudo", "style", "background: blue;");
                    page.appendAttribute("pseudo", "style", "foreground: red;");
                    page.setAttribute("pseudo", "class", "superhero");

                    String htmlTarget = page.dynamizeToString();

                    StringList display = new StringList();
                    display.append(StringEscapeUtils.escapeXml(htmlSource)).appendln("<br/>");
                    display.append("+").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent(\"pseudo\", \"spiderman\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.appendAttribute(\"pseudo\", \"style\", \"foreground: red\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.appendAttribute(\"pseudo\", \"style\", \"background: blue\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setAttribute(\"pseudo\", \"class\", \"superhero\");")).appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget)).appendln("<br/>");

                    demoPage.setContent("demo2", display.toString());
                }
                catch (Exception exception)
                {
                    demoPage.setContent("demo2", exception.getMessage());
                }
            }

            // Step #03a.
            {
                try
                {
                    StringList source = new StringList();
                    source.appendln("<ul>");
                    source.appendln("  <li id=\"words\">a word</li>");
                    source.appendln("</ul>");
                    String htmlSource = source.toString();

                    Page page = new Page(htmlSource);
                    page.addContent("words", "alpha");
                    page.addContent("words", "bravo");
                    page.addContent("words", "charlie");
                    page.addContent("words", "delta");
                    page.addContent("words", "echo");
                    page.addContent("words", "fox");

                    String htmlTarget = page.dynamizeToString();

                    StringList display = new StringList();
                    display.appendln(StringEscapeUtils.escapeXml(htmlSource).replace("\n", "<br/>\n").replace(" ", "&#160;\n"));
                    display.append("+").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.addContent (\"words\", \"alpha\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.addContent (\"words\", \"bravo\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.addContent (\"words\", \"charlie\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.addContent (\"words\", \"delta\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.addContent (\"words\", \"echo\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.addContent (\"words\", \"fox\");")).appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo3.1", display.toString());
                }
                catch (Exception exception)
                {
                    demoPage.setContent("demo3.1", exception.getMessage());
                }
            }

            // Step #03b.
            {
                try
                {
                    StringList source = new StringList();
                    source.appendln("<ul>");
                    source.appendln("  <li id=\"words\">a word</li>");
                    source.appendln("</ul>");
                    String htmlSource = source.toString();

                    // Populate.
                    Page page = new Page(htmlSource);
                    page.setContent("words", 0, "alpha");
                    page.setContent("words", 1, "bravo");
                    page.setContent("words", 2, "charlie");
                    page.setContent("words", 3, "delta");
                    page.setContent("words", 4, "echo");
                    page.setContent("words", 5, "fox");

                    String htmlTarget = page.dynamizeToString();

                    StringList display = new StringList();
                    display.appendln(StringEscapeUtils.escapeXml(htmlSource).replace("\n", "<br/>\n").replace(" ", "&#160;\n"));
                    display.append("+").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent (\"words\", 0, \"alpha\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent (\"words\", 1, \"bravo\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent (\"words\", 2, \"charlie\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent (\"words\", 3, \"delta\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent (\"words\", 4, \"echo\");")).appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml("page.setContent (\"words\", 5, \"fox\");")).appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo3.2", display.toString());
                }
                catch (Exception exception)
                {
                    demoPage.setContent("demo3.2", exception.getMessage());
                }
            }

            // Step #04.
            {
                try
                {
                    StringList source = new StringList();
                    source.append("<table>\n");
                    source.append("  <tr id='identity'><td>noid</td><td id='first_name'>Jean</td><td id='last_name'>Reve</td></tr>\n");
                    source.append("</table>");
                    String htmlSource = source.toString();

                    // Populate.
                    Page page = new Page(htmlSource);
                    page.setContent("identity", 0, "last_name", "Jemba");
                    page.setContent("identity", 0, "first_name", "Epo");
                    page.setContent("identity", 1, "last_name", "Momon");
                    page.setContent("identity", 1, "first_name", "Christian");
                    page.setContent("identity", 2, "last_name", "Tronche");
                    page.setContent("identity", 2, "first_name", "Christophe");

                    String htmlTarget = page.dynamizeToString();

                    StringList display = new StringList();
                    display.appendln(StringEscapeUtils.escapeXml(htmlSource).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");
                    display.append("+").appendln("<br/>");
                    display.append("page.setContent (\"identity\", 0, \"last_name\", \"Jemba\");").appendln("<br/>");
                    display.append("page.setContent (\"identity\", 0, \"first_name\", \"Epo\");").appendln("<br/>");
                    display.append("page.setContent (\"identity\", 1, \"last_name\", \"Momon\");").appendln("<br/>");
                    display.append("page.setContent (\"identity\", 1, \"first_name\", \"Christian\");").appendln("<br/>");
                    display.append("page.setContent (\"identity\", 2, \"last_name\", \"Tronche\");").appendln("<br/>");
                    display.append("page.setContent (\"identity\", 2, \"first_name\", \"Christophe\");").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo4", display.toString());
                }
                catch (Exception exception)
                {
                    demoPage.setContent("demo4", exception.getMessage());
                }
            }

            // Step #05.
            {
                try
                {
                    StringList source = new StringList();
                    source.appendln("<ul id=\"words\">");
                    source.appendln("  <li id=\"first\">alpha</li>");
                    source.appendln("  <li id=\"second\">bravo</li>");
                    source.appendln("  <li id=\"third\">charlie</li>");
                    source.appendln("  <li>delta</li>");
                    source.appendln("</ul>");
                    String htmlSource = source.toString();

                    // Populate.
                    Page page = new Page(htmlSource);

                    StringList display = new StringList();
                    display.appendln(StringEscapeUtils.escapeXml(htmlSource).replace("\n", "<br/>\n").replace(" ", "&#160;\n"));
                    demoPage.setContent("demo5", display.toString());

                    //
                    page.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_FIRST_ROW);
                    String htmlTarget = page.dynamizeToString();

                    display = new StringList();
                    display.append("<h3>Strategy ONLY_FIRST_ROW</h3>");
                    display.append("+").appendln("<br/>");
                    display.append("page.setIterationStrategy(\"words\", SimpleTagData.IterationStrategy.ONLY_FIRST_ROW);").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo5.1", display.toString());

                    //
                    page.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_FIRST_TWO_ROWS);
                    htmlTarget = page.dynamizeToString();

                    display = new StringList();
                    display.append("<h3>Strategy ONLY_FIRST_TWO_ROWS</h3>");
                    display.append("+").appendln("<br/>");
                    display.append("page.setIterationStrategy(\"words\", SimpleTagData.IterationStrategy.ONLY_FIRST_TWO_ROWS);").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo5.2", display.toString());

                    //
                    page.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_ROWS_WITH_ID);
                    htmlTarget = page.dynamizeToString();

                    display = new StringList();
                    display.append("<h3>Strategy ONLY_ROWS_WITH_ID</h3>");
                    display.append("+").appendln("<br/>");
                    display.append("page.setIterationStrategy(\"words\", SimpleTagData.IterationStrategy.ONLY_ROWS_WITH_ID);").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo5.3", display.toString());

                    //
                    page.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_ROWS_WITHOUT_ID);
                    htmlTarget = page.dynamizeToString();

                    display = new StringList();
                    display.append("<h3>Strategy ONLY_ROWS_WITHOUT_ID</h3>");
                    display.append("+").appendln("<br/>");
                    display.append("page.setIterationStrategy(\"words\", SimpleTagData.IterationStrategy.ONLY_ROWS_WITHOUT_ID);").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo5.4", display.toString());

                    //
                    page.setIterationStrategy("words", SimpleTagData.IterationStrategy.ALL_ROWS);
                    htmlTarget = page.dynamizeToString();

                    display = new StringList();
                    display.append("<h3>Strategy ALL_ROWS</h3>");
                    display.append("+").appendln("<br/>");
                    display.append("page.setIterationStrategy(\"words\", SimpleTagData.IterationStrategy.ALL_ROWS);").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(htmlTarget).replace("\n", "<br/>\n").replace(" ", "&#160;\n")).appendln("<br/>");

                    demoPage.setContent("demo5.5", display.toString());
                }
                catch (Exception exception)
                {
                    demoPage.setContent("demo5.1", exception.getMessage());
                }
            }

            // Step #06.
            {
                try
                {
                    Page defaultPage = new Page("/fr/devinsy/kiss4web/demo/website/xidyn/languages_demo.html");
                    Page frenchPage = new Page("/fr/devinsy/kiss4web/demo/website/xidyn/languages_demo.html", Locale.FRENCH);
                    Page germanPage = new Page("/fr/devinsy/kiss4web/demo/website/xidyn/languages_demo.html", Locale.GERMAN);

                    String defaultHtml = XidynUtils.extractBodyContent(defaultPage.dynamizeToString());
                    String frenchHtml = XidynUtils.extractBodyContent(frenchPage.dynamizeToString());
                    String germanHtml = XidynUtils.extractBodyContent(germanPage.dynamizeToString());

                    StringList display = new StringList();
                    display.appendln("<h3>No locale set</h3>");
                    display.append("new Page(\"/fr/devinsy/kiss4web/demo/website/xidyn/languages_demo.html\");").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(defaultHtml)).appendln("<br/>");
                    display.appendln("<br/>");

                    display.appendln("<h3>French locale set</h3>");
                    display.append("new Page(\"/fr/devinsy/kiss4web/demo/website/xidyn/languages_demo.html\", Locale.FRENCH);").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(frenchHtml)).appendln("<br/>");
                    display.appendln("<br/>");

                    display.appendln("<h3>German locale set without associated file</h3>");
                    display.append("new Page(\"/fr/devinsy/kiss4web/demo/website/xidyn/languages_demo.html\", Locale.GERMAN);").appendln("<br/>");
                    display.append("=>").appendln("<br/>");
                    display.append(StringEscapeUtils.escapeXml(germanHtml)).appendln("<br/>");

                    demoPage.setContent("demo6", display.toString());
                }
                catch (Exception exception)
                {
                    demoPage.setContent("demo6", exception.getMessage());
                }
            }

            //
            Page charter = PageFactory.instance().create("/fr/devinsy/kiss4web/demo/website/charter/charter.html");
            charter.include("content_container", demoPage);
            String html = charter.dynamize().toString();

            // Display page.
            response.setContentType("application/xhtml+xml; charset=UTF-8");
            response.getWriter().println(html);
        }
        catch (Exception exception)
        {
            FatalView.show(request, response, exception);
        }

        logger.debug("doGet done.");
    }

    /**
     *
     */
    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
     *
     */
    @Override
    public void init() throws ServletException
    {
    }
}

// ////////////////////////////////////////////////////////////////////////
