/**
 * Copyright (C) 2016 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web-demo.
 * 
 * Kiss4web-demo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Kiss4web-demo.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.demo.website.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.devinsy.kiss4web.demo.website.FatalView;
import fr.devinsy.xidyn.pages.Page;
import fr.devinsy.xidyn.pages.PageFactory;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Static_stuff_xhtml extends HttpServlet
{
    private static final long serialVersionUID = 3523141827401383261L;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Static_stuff_xhtml.class);
    private static URLPresenter xidyn = new URLPresenter("/fr/devinsy/kiss4web/demo/Xydin_hello.html");

    /**
	 *
	 */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
    {
        logger.debug("doGet starting...");

        try
        {
            //
            logger.debug("doGet starting...");

            // Get parameters.
            // ===============
            // Locale locale = kiwa.getUserLocale(request);
            // Long accountId = kiwa.getAuthentifiedAccountId(request,
            // response);

            // Use parameters.
            // ===============

            // Send response.
            // ==============
            Page page = PageFactory.instance().get("/fr/devinsy/kiss4web/demo/website/index.html");

            String html = page.dynamize().toString();

            // Display page.
            response.setContentType("application/xhtml+xml; charset=UTF-8");
            response.getWriter().println(html);
        }
        catch (Exception exception)
        {
            FatalView.show(request, response, exception);
        }

        logger.debug("doGet done.");
    }

    /**
	 *
	 */
    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
	 *
	 */
    @Override
    public void init() throws ServletException
    {
    }
}

// ////////////////////////////////////////////////////////////////////////
