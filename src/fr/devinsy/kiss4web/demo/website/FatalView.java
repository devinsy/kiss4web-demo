/**
 * Copyright (C) 2016 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web-demo.
 * 
 * Kiss4web-demo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Kiss4web-demo.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.demo.website;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * 
 * @author Christian P. Momon
 */
public class FatalView
{
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(FatalView.class);

    /**
     * @throws IOException
     * 
     */
    public static void show(final HttpServletRequest request, final HttpServletResponse response, final Exception source) throws IOException
    {
        // Display fatal error page.
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
        out.println("<!DOCTYPE html >");
        out.println("<html><header></header><body>");
        out.println("A fatal error occured:<br/>");
        out.println("<pre>");
        out.println(source.getMessage());
        out.println("</pre>");
        out.println("<hr />");
        if (source.getMessage() != null)
        {
            out.println("<pre>" + XidynUtils.restoreEntities(new StringBuffer(source.getMessage())) + "</pre>");
        }
        out.println("<hr />");
        out.println("</body></html>");
    }
}

// ////////////////////////////////////////////////////////////////////////
