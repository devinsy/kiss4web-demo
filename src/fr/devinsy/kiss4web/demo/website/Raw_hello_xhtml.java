/**
 * Copyright (C) 2016 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web-demo.
 * 
 * Kiss4web-demo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Kiss4web-demo.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.demo.website;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class Raw_hello_xhtml extends HttpServlet
{
    private static final long serialVersionUID = -3597088223059326129L;

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Raw_hello_xhtml.class);

    /**
	 *
	 */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
        out.println("<!DOCTYPE html >");
        out.println("<html><head></head><body>");
        out.println("Hello.");
        out.println("</body></html>");

        out.close();
    }

    /**
	 *
	 */
    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
	 *
	 */
    @Override
    public void init() throws ServletException
    {
        super.init();
    }
}

// ////////////////////////////////////////////////////////////////////////
