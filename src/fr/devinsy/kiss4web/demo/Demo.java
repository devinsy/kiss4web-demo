/**
 * Copyright (C) 2013-2016 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web-demo.
 * 
 * Kiss4web-demo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Kiss4web-demo.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.demo;

/**
 * 
 * 
 * @author Christian P. Momon
 */
public class Demo
{
    private static class SingletonHolder
    {
        private static final Demo instance = new Demo();
    }

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Demo.class);

    /**
     * @throws Exception
     * 
     */
    private Demo()
    {
    }

    /**
     * 
     * @return
     */
    public static Demo instance()
    {
        return SingletonHolder.instance;
    }
}
