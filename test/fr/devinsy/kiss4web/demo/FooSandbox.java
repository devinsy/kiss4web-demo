/**
 * Copyright (C) 2016 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web-demo.
 * 
 * Kiss4web-demo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Kiss4web-demo.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.demo;

import org.junit.Assert;
import org.junit.Test;

/**
 * Kiss4Web tests.
 */
public class FooSandbox
{
    private static org.apache.log4j.Logger logger;

    static
    {
        // Initialize logger.
        org.apache.log4j.Logger logger = null;

        org.apache.log4j.BasicConfigurator.configure();

        logger = org.apache.log4j.Logger.getRootLogger();
        // logger.setLevel (org.apache.log4j.Level.INFO);
        logger.setLevel(org.apache.log4j.Level.INFO);

        logger.info("Enter");

        //
        logger.info("Set the log file format...");

        // log =
        // org.apache.log4j.Category.getInstance(Application.class.getName());
        logger.info("... done.");

        logger.debug("Exit");
        logger = org.apache.log4j.Logger.getLogger(FooSandbox.class.getName());
    }

    /**
	 *
	 */
    @Test
    public void check()
    {
        Assert.assertTrue(true);
    }

}
