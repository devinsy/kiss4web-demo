# Kiss4web-demo

Welcome! Welcome in a world of K.I.S.S.

K.I.S.S. = Keep It Simple, Stupid (https://en.wikipedia.org/wiki/KISS_principle).

Nowadays, make a Web site involves to use complex and heavy frameworks. Nevertheless, to build a web page and to return it, does not maybe ask so much complexity.

Kiss4web is a K.I.S.S. solution for building web applications.

Kiss4web-demo is a demonstration of the use of Kiss4web.


## Developing environment

Kiss4web-demo project uses strictly Eclipse, Java 6, GIT, Tomcat 7.

### UTF-8 compliance settings
Set UTF-8 compliance when using Tomcat server directly:
- edit workspace/Servers/Tomcat v7.0 Server at localhost-config/server.xml
- add « URIEncoding="UTF-8" » to the 8080 connetor:
  <Connector connectionTimeout="20000" port="8080" protocol="HTTP/1.1" redirectPort="8443" URIEncoding="UTF-8"/>

Set UTF-8 compliance when using Tomcat server through Apache server (so using mod_jk):
- edit server.xml (on Debian, /etc/tomcat7/server.xml)
- add « URIEncoding="UTF-8" » to the AJP connetor:
  <Connector port="8009" protocol="AJP/1.3" redirectPort="8443" URIEncoding="UTF-8"/>

### Eclipse Mars tips
To get Tomcat server with Eclipse Mars, install all JST packages.

## Build

# License

Kiss4web-demo is released under the GNU AGPL license. 